# Documentation

## Tasks

### Dispatching Tasks
`SCH_Dispatch_Tasks()`

This is the 'dispatcher' function.  When a task (function)
is due to run, SCH_Dispatch_Tasks() will run it.
This function must be called (repeatedly) from the main loop.

### Adding Tasks

`SCH_Add_Task()`

Causes a task (function) to be executed at regular intervals
or after a user-defined delay

Function - The name of the function which is to be scheduled.
    NOTE: All scheduled functions must be 'void, void' -
    that is, they must take no parameters, and have
    a void return type.

DELAY - The interval (TICKS) before the task is first executed

PERIOD    - If 'PERIOD' is 0, the function is only called once,
    at the time determined by 'DELAY'.  If PERIOD is non-zero,
    then the function is called repeatedly at an interval
    determined by the value of PERIOD (see below for examples
    which should help clarify this).


RETURN VALUE:

Returns the position in the task array at which the task has been
added.  If the return value is SCH_MAX_TASKS then the task could
not be added to the array (there was insufficient space).  If the
return value is < SCH_MAX_TASKS, then the task was added
successfully.

Note: this return value may be required, if a task is
to be subsequently deleted - see SCH_Delete_Task().

EXAMPLES:

`Task_ID = SCH_Add_Task(Do_X,1000,0);`
Causes the function Do_X() to be executed once after 1000 sch ticks.

`Task_ID = SCH_Add_Task(Do_X,0,1000);`
Causes the function Do_X() to be executed regularly, every 1000 sch ticks.

`Task_ID = SCH_Add_Task(Do_X,300,1000);`
Causes the function Do_X() to be executed regularly, every 1000 ticks.
Task will be first executed at T = 300 ticks, then 1300, 2300, etc.

### Deleting Tasks

`SCH_Delete_Task()`

Removes a task from the scheduler.  Note that this does
*not* delete the associated function from memory:
it simply means that it is no longer called by the scheduler.

TASK_INDEX - The task index.  Provided by SCH_Add_Task().

RETURN VALUE:  RETURN_ERROR or RETURN_NORMAL

## Scheduler

### Initialize sceduler
`SCH_Init_T1()`

Scheduler initialisation function.  Prepares scheduler
data structures and sets up timer interrupts at required rate.
You must call this function before using the scheduler.

### Start scheduler
`SCH_Start()`

Starts the scheduler, by enabling interrupts.

NOTE: Usually called after all regular tasks are added,
to keep the tasks synchronised.

NOTE: ONLY THE SCHEDULER INTERRUPT SHOULD BE ENABLED!!!

### Update scheduler
`SCH_Update`

This is the scheduler ISR.  It is called at a rate
determined by the timer settings in SCH_Init_T1().