uint16_t sensordata = 0x0000;
//void checkAutonoom (uint16_t tmp, int sensor);



void adc_init(void){
    ADCSRA = (1<<ADEN) | (1<< ADPS0)| (1<< ADPS1)| (1<< ADPS2);  //enable and prescale = 128 (16MHz/128 = 125kHz)
}

void read_adc(uint8_t pin, int sensor){
	DDRC = 0xff & (0 << pin); //mask all input except the input pin
	//pin = pin/2;

	ADMUX = pin | (1<<REFS0) | (1<<ADLAR); //Defines the new ADC pin to be read
	ADCSRA |= (1<<ADSC);  //start the conversion
	loop_until_bit_is_set (ADCSRA , ADSC); //wait for end of conversion

	uint8_t tmpLow = ADCL;
	uint8_t tmpHigh = ADCH;
	uint16_t tmp = tmpHigh << 8 | tmpLow;

	// Send type
	SendableData = sensor;
	sendData();
	// Send value
	SendableData = tmpLow;
	sendData();
	// Send value
	SendableData = tmpHigh;
	sendData();

	checkAutonoom(tmp, sensor);

	ADCSRA |= (0<<ADSC); //stop the conversion
}

void checkAutonoom (uint16_t tmp, int sensor){
	switch (sensor) {
		case 2:
		//lightsensor
		ligthsensordata = tmp;
		break;
		case 4:
		//temp sensor
		tempsensordata = tmp;
		break;
	}

	autonoomMovement();
}
