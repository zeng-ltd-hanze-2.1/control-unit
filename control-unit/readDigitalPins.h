uint8_t readDigitalPin(uint8_t pin){
	uint8_t  output = 0x00;
	if (pin >1){
		if (pin < 8){
			DDRD = 0xFF; // Input
			output = PIND & (1 << pin); //mask all input except the input pins
			output = output /4;
		} else if (pin < 14) {
			pin = pin - 8;
			DDRB = 0x00; // Input
			output =PINB & (1 << pin); //mask all input except the input pins
		}
	}
	return output;
}

/*NOTE: this is for testing purposes*/
uint8_t readDigitalPins(){
	uint8_t output;
	//2-7 can be read at the same time
	output = readDigitalPin(2)|readDigitalPin(7);
	//8-13 can be read at the same time
    return output;
}
