#include <util/delay.h>
volatile int greenStatusLedPin = 10 ;
volatile int redStatusLedPin = 11;
volatile int yellowStatusLedPin = 12;


void switchYellowStatusLed(){
	writeDigitalPort(yellowStatusLedPin, ~readDigitalPin(yellowStatusLedPin));
}

void StatusLedOn(){
	writeDigitalPort(greenStatusLedPin , 0);
	writeDigitalPort(redStatusLedPin , 0);
	writeDigitalPort(yellowStatusLedPin, 0);
}

void StatusLedOff(){
	writeDigitalPort(greenStatusLedPin , 0);
	writeDigitalPort(redStatusLedPin , 1);
	writeDigitalPort(yellowStatusLedPin, 0);
}

void StatusMove(){
	writeDigitalPort(greenStatusLedPin , 0);
	writeDigitalPort(redStatusLedPin , 0);
	SCH_Add_Task(switchYellowStatusLed, 500, 0);
}
