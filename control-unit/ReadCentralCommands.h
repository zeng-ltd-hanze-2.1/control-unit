enum input {movescreen, setVariable, unknown};
enum variablesToSettype {hard, soft};
enum variablesToSet {templimit, lightlimit};
input = unknown;

void ReadCentralCommands(uint8_t Data){
	if (input == unknown){
		//set what needs to be done
		switch (Data) {
			case 0x01:
			input = movescreen;
			break;
			case 0x02:
			input = setVariable;
			break;
			default:
			input = unknown;
			//send some soort of error message
		}
		SendableData = input;
		sendData();
	}else {

		switch (input) {

			case movescreen:
				switch (Data) {
					//move up down stop
					case 0x01:
					status = move;
					direction = up;
					break;
					case 0x02:
					status = move;
					direction = down;
					break;
					case 0x03:
					status = stop;
					break;
					default:
					//send error
					status = sconfig;
					direction = dconfig;
				}
			ScreenUpdate();
			break;

			case setVariable:
				switch (Data) {
					case 0x01:
					break;
				}
		}
		input = unknown;
	}
}
