uint8_t IgnoreBaseStationButtonPin = 14;
enum buttonstatus {on,off};
buttonstatus = off;

void switchRxInterrupt () {
	if (buttonstatus == off){
			//switch allow input on rx and isr call on rx
			UCSR0B ^= (1 << RXEN0)|(1 << RXCIE0);
			//notify basestation that we do not listen to it
			SendableData = 0x10;
			sendData();
			SendableData = UCSR0B & 0b00010000; //sends 0x10 if we are receiving data /sends 0x00 if we are ignoring the basestation
			sendData();
			buttonstatus = on;
	}
}

void ReadIgnoreBaseStationButton(){
	if (IgnoreBaseStationButtonPin != 0x00){
		if (IgnoreBaseStationButtonPin < 8){
			if (readDigitalPin(IgnoreBaseStationButtonPin) && (1<<(IgnoreBaseStationButtonPin-2))){
				switchRxInterrupt();
			}else{
			buttonstatus = off;
			}
		}else if(IgnoreBaseStationButtonPin < 14){
			if (readDigitalPin(IgnoreBaseStationButtonPin) & (1<<IgnoreBaseStationButtonPin -8)){
				switchRxInterrupt();

			}else{
			buttonstatus = off;
			}
		}
	}

}
