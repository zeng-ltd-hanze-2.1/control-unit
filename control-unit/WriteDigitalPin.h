void writeDigitalPort(uint8_t pin,int status){
	if (pin >1){//skip pin 0 and 1 (rx tx)
		if (pin < 8){
			DDRD = 0xFF; // set as output
			PORTD = (status << pin); //turn on pin
		} else if (pin < 14) {
			pin = pin - 8;
			DDRB = 0xFF; //set as output
			PORTB = (status << pin); //turn on pin
		}
	}
}
