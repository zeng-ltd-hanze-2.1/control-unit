volatile uint8_t SendableData = 0x7D;

void usart_init(){
	#define  UBBRVAL 51;
	//set the baud rate
	UBRR0H=0 ; //set standard baud rate 19.2k
	UBRR0L= UBBRVAL;
	//disable U2X mode
	UCSR0A=0;
	//enable transmitter and receive enable interrupt on RX
	UCSR0B = (1 << TXEN0) | (1 << RXEN0) | (1 << RXCIE0);
	// set frame format : asynchronous, 8 data bits, 1 stop bit, no parity
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);
}

//send data via usb
void sendData(){
	//wait for an empty transmit buffer
	//UDRE is set when the transmit buffer is empty
	loop_until_bit_is_set(UCSR0A,UDRE0);
	//send the data
	UDR0=SendableData;
}

//receive data via usb
uint8_t ReceiveData(){
	loop_until_bit_is_set(UCSR0A,RXC0);
	return UDR0;
}

ISR(USART_RX_vect){
  //receive data when it comes in
	SendableData = ReceiveData();
	sendData();
}
