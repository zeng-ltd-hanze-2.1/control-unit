#include "includer.h"

void init(){
	SCH_Init_T1();// initialization of scheduler
	usart_init();// initialization of rx tx handling for transmitting data
	adc_init(); // initialization of the analog digital converter
	sonar_init(); // initialize sonar
}


int main(){
	init();//initialization of all items

	//add the tasks to be done
	//sch_add_task period 1 second = 2000 PERIOD
	//SCH_Add_Task(ReadIgnoreBaseStationButton, 0 ,20);
	SCH_Add_Task(read_light_sensor,0,2000); //elke 40sec lees
	SCH_Add_Task(read_temp_sensor,10,2000);//elke 40sec lees
  SCH_Add_Task(ScreenButtonUpdate, 20,20);

	SCH_Start();// runs scheduler

	while(1){
		SCH_Dispatch_Tasks();
	}
	return 0;
}
