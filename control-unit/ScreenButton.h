uint8_t ScreenButtonPin = 2 ; //set on what button this code should run

enum status {top,bottom,move,stop,sconfig};
enum direction {up,down,dconfig};
status = sconfig;
direction = dconfig;

void ScreenUpdate(){
	switch (status) {
		case sconfig:
		//do after a Reset
		// check if screen is up else move screen to top

		//TODO: read out the ultrasonic sensor and check if it is at the top
		if (status != top){
			// move the screen until the screen is at the top
			direction = up;
			status = move;
			ScreenUpdate();
		}

		break;
		case move:
		//start moving
		//TODO start moving
		StatusMove();

		break;
		case stop:
			//switch direction
			switch (direction) {
				case up:
					direction = down;
				break;
				case down:
					direction = up;
				break;
			}
		break;
		case top:
			//stop moving
			StatusLedOff();
			direction = down;
			break;
		case bottom:
			//stop moving
			StatusLedOn();
			direction = up;
			//set direction to up
		break;


	}

	//report back the status of the screen to base station
	SendableData = 0x01; //tell base station its getting the status
	sendData();
	//send status
	SendableData = status;
	sendData();

	SendableData = direction;
	sendData();


}

void ScreenButtonUpdate(){
	if (readDigitalPin(ScreenButtonPin) == 0x00){
		//did not return anny button being on
		//do not run code
	}else if (ScreenButtonPin < 8 ){
		if (readDigitalPin(ScreenButtonPin) & (1<<ScreenButtonPin-2)){ //check if the button is on /-2 offsets the tx and rx pin
            switch (status) {
                case move:
                    status = top;
                break;
                case top:
                    status = bottom;
                break;
                default:
                    status =move;
            }
			ScreenUpdate();
		}
	}else if (ScreenButtonPin <14 ){
		if (readDigitalPin(ScreenButtonPin) & (1<<ScreenButtonPin-8)){ //check if the button is on /-8 offsets to the correct pin
            switch (status) {
                case move:
                    status = top;
                break;
                case top:
                    status = bottom;
                break;
                default:
                    status =move;
            }
			ScreenUpdate();
		}
	}
}
