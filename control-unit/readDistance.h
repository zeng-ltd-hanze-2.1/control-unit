#include <util/delay.h>

int echo_received = 0;
float counter = 0.0;
int sonarWaarde  = 0;


void sonar_init(){
    DDRB = 0b00000001;

    //Enable Pin Change Interrupt on PCINT0 digital pin 8 and 9
    // todo: set these to be variable
    PCMSK0 |= (1 << PCINT1);
    PCICR |= (1 << PCIE0);
    sei();
}

void start_timer(){
    // set CTC on
    TCCR0A  = (1 << WGM01);
    // count to 255
    OCR0A = 255;
    // enable
    TIMSK0 = (1 << OCIE0A);
    sei();
    // prescaler to 0, every 0.0000159375 (15.9 microseconds)
    TCCR0B = (0 << CS02) | (0 << CS01) | (1 << CS00);
}

void read_distance_cm(){
    PORTB = 0b00000001;
    _delay_us(10);
    PORTB = 0b00000000;

    start_timer();
    // send identifier
    SendableData = 0x08;
    sendData();//sends the sensor data 8msb over serial

    // send 2 empty bits
    SendableData = 0x00;
    sendData();//sends the sensor data 8msb over serial

    // send cm
    SendableData = sonarWaarde / 58;
    sendData();//sends the sensor data 8msb over serial
}

ISR(PCINT0_vect){
    TCCR0A = 0; // timer uit
    sonarWaarde  = counter;
    counter = 0;
}

ISR(TIMER0_COMPA_vect){
    counter = counter + 15.9;
}
