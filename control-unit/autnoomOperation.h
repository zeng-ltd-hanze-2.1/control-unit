//2 hard limits for temp en light sensor
//2 soft limits for temp en light sensor
uint16_t HardTopLightLimit = 1000;
uint16_t HardTopTempLimit =  1000;

uint16_t SoftTopLightLimit = 600;
uint16_t SoftTopTempLimit =  600;


uint16_t HardBottomLightLimit = 0;
uint16_t HardBottomTempLimit =  0;

uint16_t SoftBottomLightLimit = 100;
uint16_t SoftBottomTempLimit =  100;


uint16_t ligthsensordata = 500;
uint16_t tempsensordata =  500;


void autonoomMove(int way){
	//move screen down
	direction = way;
	status = move;
	ScreenUpdate();
}

void autonoomMovement(){
	//if UCSR0B & 0b00010000 is not set ignore the sensors
	//check if base station is being ignored
	if ((UCSR0B & 0b00010000)){
		//check if hard limit is met if so lower screen
		if (tempsensordata >= HardTopTempLimit | ligthsensordata >= HardTopLightLimit){
			autonoomMove(down);
		}
		//check if soft limits both met if so move screen
		else if (tempsensordata >=SoftTopTempLimit & ligthsensordata >= SoftTopLightLimit){
			autonoomMove(down);
		} else if (tempsensordata <= HardBottomTempLimit | ligthsensordata <= HardBottomLightLimit){
			autonoomMove(up);
		}
		//check if soft limits both met if so move screen
		else if (tempsensordata <=SoftBottomTempLimit & ligthsensordata <= SoftBottomLightLimit){
			autonoomMove(up);
		}
	}
	//else keep screen where it is*/
}
